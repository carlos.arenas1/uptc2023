package edu.uptc.tests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CrearCuentaUsuario {
	WebDriver driver;
	
	@Test
    public void crearCuenta() {
		WebDriverManager.chromedriver().setup();
        ChromeOptions options=new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1920,1200");
        driver=new ChromeDriver(options);
        // Navegar a la p�gina de Lumay
        driver.get("https://magento.softwaretestingboard.com/");

        // Hacer clic en "Create an Account"
        WebElement createAccountLink = driver.findElement(By.linkText("Create an Account"));
        createAccountLink.click();

        // Rellenar los campos del formulario
        WebElement firstNameField = driver.findElement(By.id("firstname"));
        firstNameField.sendKeys("John");

        WebElement lastNameField = driver.findElement(By.id("lastname"));
        lastNameField.sendKeys("Doe");

        WebElement emailField = driver.findElement(By.id("email_address"));
        emailField.sendKeys("john.doe@yooopmail.com");

        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("Password123!");

        WebElement confirmPasswordField = driver.findElement(By.id("password-confirmation"));
        confirmPasswordField.sendKeys("Password123!");

        // Hacer clic en "Submit"
        //WebElement submitButton = driver.findElement(By.cssSelector(".buttons-set > button"));
        WebElement submitButton = driver.findElement(By.xpath("//button [contains( @title, 'Create an Account')]"));
        submitButton.click();

        // Comprobar que se ha creado la cuenta correctamente
        //WebElement successMessage = driver.findElement(By.className("success-msg"));
        //WebElement successMessage = driver.findElement(By.className("base"));
        //WebElement successMessage = driver.findElement(By.className("block-title"));
        WebElement successMessage = driver.findElement(By.xpath("//span [contains( @class, 'base')]"));
        String message = successMessage.getText();
        Assert.assertEquals(message,"My Account");
        System.out.println("La cuenta se ha creado correctamente.");

        // Cerrar el navegador
        driver.quit();
    }
}
